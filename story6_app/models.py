from django.db import models
from django.utils.timezone import now

# Create your models here.
class Status(models.Model):
    status = models.CharField(max_length=300)
    datetime = models.DateTimeField(default=now, blank=True)

class Subscriber(models.Model):
    nama = models.CharField(max_length=100)
    email = models.EmailField(max_length=100)
    password = models.CharField(max_length=100)
