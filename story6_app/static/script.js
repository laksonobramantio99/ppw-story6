// --------------- script register.html ------------------

$(document).ready(function() {

    var nama_valid = false;
    var email_valid = false;
    var password_valid = false;

    // Fungsi ngecek field nama tidak kosong
    $('#id_nama').on('blur', function() {
        var isi = $(this).val();
        console.log(nama_valid);

        if (isi === '') {
            nama_valid = false;
        }
        else{
            nama_valid = true;
        }
        console.log(nama_valid);
        cekButton();
    });

    // Fungsi ngecek field password tidak kosong
    $('#id_password').on('blur', function() {
        var isi = $(this).val();
        console.log(password_valid);

        if (isi === '') {
            password_valid = false;
        }
        else{
            password_valid = true;
        }
        console.log(password_valid);
        cekButton();
    });

    // Fungsi ngecek field email tidak kosong dan benar
    $('#id_email').on('blur', function() {
        var isi = $(this).val();
        console.log(isi);
        var email_kosong = true;
        var email_identik = false

        if (isi !== '') {
            email_kosong = false;
        } else {
            email_kosong = true;
        }
        console.log(email_kosong);

        $.ajax( {
            type: 'GET',
            url: '/validate/' + isi + '/',
            success: function(data) {
                console.log(data.status);
                email_identik = !(data.status);

                if (data.status == true) {
                    $('#message-info').html(data.message);
                } else {
                    $('#message-info').html('');
                }

                console.log(email_identik);

                if (!email_kosong && email_identik) {
                    email_valid = true;
                }
                else {
                    email_valid = false;
                }
                console.log(email_valid);

                cekButton();
            },
        })
    });

    function cekButton() {
        if (nama_valid && email_valid && password_valid) {
            $('#button-submit').attr('disabled', false);
        }
        else {
            $('#button-submit').attr('disabled', true);
        }
    }



    $('#button-submit').on('click', function(e) {
        e.preventDefault();
        var nama = $("#id_nama").val();
        var email = $("#id_email").val();
        var password = $("#id_password").val();

        var inputan = {'nama': nama, 'email': email, 'password': password};

        $.ajax( {
            method: 'POST',
            url: '/subscribe/',
            data: inputan,
            success: function(data) {
                console.log(data.status);
                if (data.status) {
                    $("#id_nama").val('');
                    $("#id_email").val('');
                    $("#id_password").val('');
                    $("#subscibe-info").html('Successfully subscribe.');
                }
                else {
                    $("#subscibe-info").html('!! Failed to subscribe, the input is invalid !!');
                }
            },
        })

    });
});

// -------------------------------------------------------


// --------------- script books.html ------------------

var counter = 0;

function change(key) {
    var $tablebody = $("#table-body");
    $.ajax( {

        type: 'GET',
        url: "/api_books/" + key + "/",
        success: function(data) {
            $("#table-body").html('');
            counter = 0;
            $("#favorite-counter").html('Favorite: '+counter);

            console.log('success', data);
            $.each(data.items, function(i, book) {
                $tablebody.append("<tr>");
                $tablebody.append('<th scope="row">'+ (i+1) + '</th>');
                $tablebody.append('<td><img src="'+ book.volumeInfo.imageLinks.thumbnail + '"></td>');
                $tablebody.append('<td>'+ book.volumeInfo.title + '</td>');
                $tablebody.append('<td>'+ book.volumeInfo.authors + '</td>');
                $tablebody.append('<td><button type="button" class="btn btn-light button-star"><i class="fa fa-star-o" style="font-size:40px;color:yellow"></i></button></td>');
                $tablebody.append("</tr>");
            });
        },
        error: function() {
            alert('error while loading the data');
        }
    });
}


$(document).ready(function() {
    var star = ["fa fa-star-o", "fa fa-star"];
    $('body').on('click', 'i', function() {
        var $status = $(this).attr('class');
        if ($status == star[0]) {
            $(this).attr('class', star[1]);
            counter++;
            console.log(counter);
        }
        else {
            $(this).attr('class', star[0]);
            counter--;
            console.log(counter);
        }
        //////// buat update star
        $("#favorite-counter").html('Favorite: '+counter);
    })
});

// ------------------------------------------------


$(document).ready(function(){
    $("#default").click(function(){
        $(".main-page").animate({backgroundColor: "#d6d5d5", color: "black"});
    })
});

$(document).ready(function(){
    $("#nightMode").click(function(){
        $(".main-page").animate({backgroundColor: "#2c3e50", color: "white"});
    })
});

$( function() {
  $( "#accordion" ).accordion({
    collapsible: true
  });
} );

$(function() {
    $(".preload").fadeOut(1000, function() {
        $(".main-page").fadeIn(1000);
    });
});
