from django import forms
from .models import *

class FormStatus(forms.ModelForm):
    status = forms.CharField(max_length=300)

    class Meta:
        model = Status
        fields = ('status', )

class FormSubscriber(forms.ModelForm):
   
    nama = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'class' : 'form-control'}))
    email = forms.EmailField(max_length=100, widget=forms.TextInput(attrs={'class' : 'form-control'}))
    password = forms.CharField(max_length=100, widget=forms.PasswordInput(attrs={'class' : 'form-control'}))

    class Meta:
        model = Subscriber
        fields = ('nama','email','password')
            

    
        
