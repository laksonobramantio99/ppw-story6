from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.utils.timezone import now
from .views import index
from .forms import FormStatus
from .models import Status

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class VisitorTest(TestCase):

    def test_if_hello_is_exist(self):
        test = "Hello, Apa kabar?"
        response= Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_if_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_if_using_index_function(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_if_using_index_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'index.html')

    def test_if_url_is_not_exist(self):
        response = Client().get('/wek')
        self.assertEqual(response.status_code, 404)

    def test_form_validation_for_blank_items(self):
            form = FormStatus(data={'status' : ''})
            self.assertFalse(form.is_valid())
            self.assertEqual(
                form.errors['status'],
                ["This field is required."]
            )

    def test_models_if_on_the_database(self):
        new_activity = Status.objects.create(status='lagi ngerjain story ppw nih!', datetime=now())

        counting_all_available_todo = Status.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)

    def test_if_post_success_and_render_the_result(self):
        test = 'Lagi makan nih'
        response_post = Client().post('/', {'status' : test, 'datetime' : now})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_if_post_failed_and_render_the_result(self):
        test = 'Lagi makan nih'
        response_post = Client().post('/', {'status' : '', 'datetime' : ''})
        self.assertEqual(response_post.status_code, 200)

        response= Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_if_myname_exist(self):
        test = "Laksono Bramantio"
        response= Client().get('/profile/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_if_url_profile_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_if_using_profile_template(self):
        response = Client().get('/profile/')
        self.assertTemplateUsed(response, 'profile.html')
        

class FunctionalTest(TestCase):

    def setUp(self):
        super(FunctionalTest, self).setUp()
        chrome_options = webdriver.chrome.options.Options()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        self.selenium  = webdriver.Chrome(chrome_options=chrome_options)
        self.selenium.implicitly_wait(25)

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()

    def test_input_todo(self):
        # Opening the link we want to test
        self.selenium.get('http://localhost:8000/')
        
        # find the form element
        status = self.selenium.find_element_by_id('id_status')
        submit = self.selenium.find_element_by_name('submit')
        
        # Fill the form with data
        status.send_keys('Coba Coba')

        # submitting the form
        status.send_keys(Keys.RETURN)

        # check "Coba Coba" is in page
        self.assertIn('Coba Coba', self.selenium.page_source)

    def test_the_title_is_true(self):
        self.selenium.get('http://ppw-f-story6-tio.herokuapp.com/')
        self.assertEqual("Statusku", self.selenium.title)

    def test_the_page_is_using_bootstrap4(self):
        self.selenium.get('http://ppw-f-story6-tio.herokuapp.com/')
        style = self.selenium.find_element_by_tag_name('link')
        self.assertEqual("stylesheet", style.get_attribute("rel"))
        self.assertEqual("https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css", style.get_attribute("href"))

    def test_status_field_style(self):
        self.selenium.get('http://ppw-f-story6-tio.herokuapp.com/')
        form_group = self.selenium.find_element_by_id('div_id_status')
        self.assertEqual("form-group", form_group.get_attribute("class"))

    def test_button_style(self):
        self.selenium.get('http://ppw-f-story6-tio.herokuapp.com/')
        button = self.selenium.find_element_by_name('submit')
        self.assertEqual("btn btn-secondary text-center", button.get_attribute("class"))
        