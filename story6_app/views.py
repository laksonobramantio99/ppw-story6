from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from story6_app.models import Status, Subscriber
from . import forms
import requests
import json
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
def index(request):
    if request.method == "POST":
        form = forms.FormStatus(request.POST)
        if form.is_valid():
            form.save()
            return redirect('index')
    else:
        form = forms.FormStatus()

    status_list = Status.objects.all()
    return render(request, 'index.html', {'form' : form, 'status' : status_list})

def profile(request):
    return render(request, "profile.html")

def books_views(request):
    return render(request, "books.html")

def api_books_views(request, key=None):
    data = requests.get("https://www.googleapis.com/books/v1/volumes?q=" + key)
    json_api = data.json()
    return JsonResponse(json_api)

def register_views(request):
    form = forms.FormSubscriber()
    response = {'form' : form}
    return render(request, 'register.html', response)

def validate(request, email=None):
    email_status = Subscriber.objects.filter(email__iexact = email).exists()
    status = {'status': email_status}

    if status['status']:
        status['message'] = 'This email has already taken, please register with another email.'
    else:
        status['message'] = 'This email is available.'

    text = json.dumps(status)
    return HttpResponse(text, content_type='application/json')

@csrf_exempt
def add_subs(request):
    response = {}

    if request.method == 'POST':
        data = forms.FormSubscriber(request.POST)
        if data.is_valid():
            data.save()        
            response['status'] = True
        else:
            response['status'] = False

        data_json = json.dumps(response)
        return HttpResponse(data_json, content_type='application/json')
