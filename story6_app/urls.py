from django.urls import path
from django.conf.urls import url
from .views import *

urlpatterns = [
    path('', index, name='index'),
    path('profile/', profile, name='profile'),
    path('books/', books_views, name='books'),
    path('api_books/<key>/', api_books_views, name='api_books'),
    path('register/', register_views, name='register'),
    path('validate/<email>/', validate, name='validate'),
    path('subscribe/', add_subs, name='add_subs'),
]

