// --------------- script register.html ------------------

$.(document).ready(function() {
    $.('#form-register').on('blur', function() {
        var email = $("#id_email").val();
        $.ajax( {
            type: 'GET',
            url: '/validate/' + email + '/',
            success: function(data) {
                if (data.status) {
                    console.log(data.message)
                    $('#message-info').html(data.message);
                }
                else {
                    $('#message-info').html(data.message);
                }
            },
            error: function() {
                alert('error');
            }
        })
    })
});

// -------------------------------------------------------


// --------------- script books.html ------------------

var counter = 0;

function change(key) {
    var $tablebody = $("#table-body");
    $.ajax( {

        type: 'GET',
        url: "/api_books/" + key + "/",
        success: function(data) {
            $("#table-body").html('');
            counter = 0;
            $("#favorite-counter").html('Favorite: '+counter);

            console.log('success', data);
            $.each(data.items, function(i, book) {
                $tablebody.append("<tr>");
                $tablebody.append('<th scope="row">'+ (i+1) + '</th>');
                $tablebody.append('<td><img src="'+ book.volumeInfo.imageLinks.thumbnail + '"></td>');
                $tablebody.append('<td>'+ book.volumeInfo.title + '</td>');
                $tablebody.append('<td>'+ book.volumeInfo.authors + '</td>');
                $tablebody.append('<td><button type="button" class="btn btn-light button-star"><i class="fa fa-star-o" style="font-size:40px;color:yellow"></i></button></td>');
                $tablebody.append("</tr>");
            });
        },
        error: function() {
            alert('error while loading the data');
        }
    });
}


$(document).ready(function() {
    var star = ["fa fa-star-o", "fa fa-star"];
    $('body').on('click', 'i', function() {
        var $status = $(this).attr('class');
        if ($status == star[0]) {
            $(this).attr('class', star[1]);
            counter++;
            console.log(counter);
        }
        else {
            $(this).attr('class', star[0]);
            counter--;
            console.log(counter);
        }
        //////// buat update star
        $("#favorite-counter").html('Favorite: '+counter);
    })
});

// ------------------------------------------------


$(document).ready(function(){
    $("#default").click(function(){
        $(".main-page").animate({backgroundColor: "#d6d5d5", color: "black"});
    })
});

$(document).ready(function(){
    $("#nightMode").click(function(){
        $(".main-page").animate({backgroundColor: "#2c3e50", color: "white"});
    })
});

$( function() {
  $( "#accordion" ).accordion({
    collapsible: true
  });
} );

$(function() {
    $(".preload").fadeOut(1000, function() {
        $(".main-page").fadeIn(1000);
    });
});
